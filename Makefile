MIX_ENV ?= test
COMPILE_FLAGS ?= --warnings-as-errors

ORG=arikai
VERSION ?= $(shell cat VERSION)
RELEASE_DATE ?= $(shell date -I)
COMMIT = $(shell git rev-parse HEAD)

APP ?= carbon_watcher
REGISTRY ?= registry.gitlab.com

MIX = $(shell which mix 2>/dev/null)

IMAGE_TAG ?= $(REGISTRY)/$(ORG)/$(APP):$(VERSION)

.PHONY: build_tools format deps compile force_compile check_no_unused_deps		\
				check_format test coverage wc_test wc_coverage migrate check_dialyzer \
				check_style check_docs release build_image push_image

all:
	docker-compose run --rm app eval "CarbonWatcher.Release.migrate"
	docker-compose up app

all_build:
	docker-compose build app_build
	docker-compose run --rm app_build eval "CarbonWatcher.Release.migrate"
	docker-compose up app_build

all_checks: deps compile check_no_unused_deps check_format \
						wc_test check_dialyzer check_style check_docs

build_tools:
	$(MIX) local.hex --force
	$(MIX) local.rebar --force

format:
	$(MIX) format

deps:
	$(MIX) deps.get --only $(MIX_ENV)

compile:
	$(MIX) compile $(COMPILE_FLAGS)

force_compile:
	$(MIX) compile $(COMPILE_FLAGS) --force

check_no_unused_deps:
	$(MIX) deps.unlock --check-unused

check_format:
	$(MIX) format --check-formatted

test:
	$(MIX) test --cover --warnings-as-errors

wc_test:
	docker-compose run --rm test make deps migrate test MIX_ENV=test; docker-compose rm --stop --force timescaledb_test

migrate:
	MIX_ENV=$(MIX_ENV) $(MIX) ecto.migrate --all --log-sql --strict-version-order

check_dialyzer:
	MIX_ENV=$(MIX_ENV) $(MIX) dialyzer

check_style:
	$(MIX) credo --color --all-priorities

check_docs:
	$(MIX) doctor  --full --raise

release:
	$(MIX) release --overwrite

build_image:
	docker build --no-cache \
    --tag $(IMAGE_TAG) \
    --build-arg ORG=$(ORG)         \
		--build-arg	VERSION=$(VERSION) \
		--build-arg RELEASE_DATE=$(RELEASE_DATE) \
		--build-arg COMMIT=$(COMMIT) \
		.

push_image:
	docker push $(IMAGE_TAG)
