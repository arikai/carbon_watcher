defmodule CarbonWatcher.MixProject do
  use Mix.Project

  def project do
    [
      app: :carbon_watcher,
      version: version(),
      elixir: "~> 1.12",
      start_permanent: Mix.env() == :prod,
      test_coverage: [tool: ExCoveralls],
      deps: deps(),
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test
      ],
      releases: releases()
    ]
  end

  def version() do
    "VERSION"
    |> File.read!()
    |> String.trim()
  end

  def application do
    [
      extra_applications: [:logger],
      mod: {CarbonWatcher.Application, []}
    ]
  end

  defp deps do
    [
      # DB
      {:ecto, "~> 3.7"},
      {:ecto_sql, "~> 3.7"},
      {:postgrex, "~> 0.15"},

      # Job queue
      {:oban, "~> 2.8"},

      # Web
      {:mint, "~> 1.3"},
      {:castore, "~> 0.1.0"},
      {:jason, "~> 1.2"},

      # Other utils
      {:timex, "~> 3.5"},

      # Test dependencies
      {:plug_cowboy, "~> 2.5", only: :test},

      # Checkers and Linters
      {:dialyxir, "~> 1.1", only: [:dev, :test], runtime: false},
      {:credo, "~> 1.5", only: [:dev, :test], runtime: false},
      {:doctor, "~> 0.18.0", only: [:dev, :test], runtime: false},
      {:excoveralls, "~> 0.14", only: [:test]}
    ]
  end

  defp releases do
    [
      carbon_watcher: [
        include_executables_for: [:unix]
      ]
    ]
  end
end
