FROM hexpm/elixir:1.12.3-erlang-24.0.6-debian-buster-20210902-slim as build

ENV MIX_ENV=prod

WORKDIR /build

RUN apt-get install make -qq --yes

COPY VERSION Makefile ./
COPY mix.exs mix.lock ./
COPY config config

RUN make build_tools
RUN make deps
RUN make compile

COPY lib lib
COPY priv priv
RUN mix compile

COPY rel rel
RUN mix release

# ------------------------------------------------------------------------------

FROM debian:buster-20210326-slim as app
ARG VERSION
ARG RELEASE_DATE
ARG COMMIT

ENV LC_ALL C.UTF-8

RUN apt-get update && \
    apt-get install make -qq --yes openssl && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /app

COPY --from=build /build/_build/prod/rel/carbon_watcher ./

ENTRYPOINT ["/app/bin/carbon_watcher"]
CMD ["start"]

LABEL maintainer="Yaroslav Rogov <rogovyaroslav@gmail.com>" \
      version="${VERSION}"                                  \
      release-date="${RELEASE_DATE}"                        \
      commit="${COMMIT}"
