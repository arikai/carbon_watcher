defmodule CarbonWatcher.Repo.Migrations.AddCarbonIntensityTable do
  use Ecto.Migration

  @table "carbon_intensity"
  def change do
    create table(@table, primary_key: false) do
      add :timestamp, :timestamp, null: false
      add :value, :integer, null: false
    end

    create index(@table, [:timestamp], unique: true)

    # Create TimescaleDB hypertable
    execute(
      "SELECT create_hypertable('#{@table}', 'timestamp')",
      ""
    )
  end
end
