defmodule CarbonWatcher.SyncWorker do
  @moduledoc "Main Oban worker for syncing carbon intensity data"

  @max_attempts 20
  use Oban.Worker,
    queue: :sync,
    priority: 3,
    max_attempts: @max_attempts,
    unique: [period: :infinity]

  alias CarbonWatcher.SyncUtils

  @impl Oban.Worker
  def perform(job = %{attempt: @max_attempts}) do
    CarbonWatcher.ObanUtils.reset_oban_attempts(__MODULE__, job)
  end

  def perform(%{args: %{"at" => datetime_str}}) do
    with {:ok, datetime_raw} <- NaiveDateTime.from_iso8601(datetime_str),
         datetime <- SyncUtils.clip_datetime(datetime_raw),
         {:ok, carbon_intensity} <- CarbonWatcher.Api.get_actual_intensity_at_datetime(datetime) do
      CarbonWatcher.Measurements.save_actual_intensity(datetime, carbon_intensity)
    end
  end
end
