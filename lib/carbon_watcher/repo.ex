defmodule CarbonWatcher.Repo do
  use Ecto.Repo,
    otp_app: :carbon_watcher,
    adapter: Ecto.Adapters.Postgres
end
