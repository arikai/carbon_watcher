defmodule CarbonWatcher.Measurements do
  require Logger

  alias CarbonWatcher.Repo

  def save_actual_intensity(datetime, carbon_intensity) do
    Ecto.Adapters.SQL.query(
      Repo,
      "INSERT INTO carbon_intensity(timestamp, value) VALUES ($1, $2)",
      [datetime, carbon_intensity]
    )
    |> case do
      {:error, %Postgrex.Error{postgres: %{code: :unique_violation}}} ->
        # NOTE: The only way this condition can be triggered is by changing database in any other way.
        #       Although the situation is exceptional, logging this won't hurt
        Logger.warn("Data point #{datetime} is already present: skipping fetched data")
        :ok

      other ->
        other
    end
  end
end
