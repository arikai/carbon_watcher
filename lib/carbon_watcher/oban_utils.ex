defmodule CarbonWatcher.ObanUtils do
  require Logger

  # HACK: Since Oban hardcaps max_attempts at 20, it's easier to exploit this hack for jobs with infinite attempts than
  #       to periodically check the whole measurements table for missing data points
  #       and later deal with long-failing jobs appropriately
  # NOTE: Because args do differ in "previous_attempts" arg between different cycles,
  #       uniqueness doesn't collapse them, and therefore, the hack works correctly
  def reset_oban_attempts(module, %Oban.Job{
        attempt: attempt,
        inserted_at: inserted_at,
        args: args
      }) do
    args
    |> Map.put_new("actual_inserted_at", inserted_at)
    |> Map.update("previous_attempts", attempt, &(&1 + attempt))
    |> module.new()
    |> Oban.insert()
    |> case do
      {:ok, _} ->
        {:error, :max_attempts_wraparound}

      {:error, _} = error ->
        Logger.alert(
          "Failed to wrap around the job #{module} for #{args}: data point might be missing"
        )

        error
    end
  end
end
