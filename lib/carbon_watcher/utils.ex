defmodule CarbonWatcher.Utils do
  def list_fetch_at(list, index) do
    case Enum.at(list, index) do
      nil -> raise KeyError, term: list, key: index
      value -> value
    end
  end
end
