defmodule CarbonWatcher.Application do
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children =
      [
        CarbonWatcher.Repo,
        {Oban, oban_config()}
      ] ++ backsync_children()

    opts = [
      strategy: :one_for_one,
      name: CarbonWatcher.Supervisor
    ]

    Supervisor.start_link(children, opts)
  end

  if Mix.env() == :test do
    defp backsync_children, do: []
  else
    defp backsync_children, do: [CarbonWatcher.BacksyncPlanner]
  end

  defp oban_config do
    Application.fetch_env!(:carbon_watcher, Oban)
  end
end
