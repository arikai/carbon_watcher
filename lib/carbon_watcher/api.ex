defmodule CarbonWatcher.Api do
  @moduledoc "API to National Grid's Carbon Intensity"

  alias Mint.HTTP
  alias CarbonWatcher.ApiUtils

  @doc """
  Request actual intensity data from API
  """
  @spec get_intensity_at_datetime(NaiveDateTime.t()) :: {:ok, integer()} | {:error, any()}
  def get_actual_intensity_at_datetime(datetime) do
    with {:ok, response} <- CarbonWatcher.Api.get_intensity_at_datetime(datetime),
         {:ok, carbon_intensity} <- CarbonWatcher.ApiUtils.extract_actual_intensity(response) do
      {:ok, carbon_intensity}
    end
  end

  @doc """
  Request intensity data from API
  """
  @spec get_intensity_at_datetime(NaiveDateTime.t()) :: {:ok, map()} | {:error, any()}
  def get_intensity_at_datetime(datetime = %NaiveDateTime{}) do
    date = NaiveDateTime.to_date(datetime)
    period = ApiUtils.to_period(datetime)

    call("GET", "/intensity/date/#{date}/#{period}")
  end

  defp call(method, endpoint) do
    uri = base_url()
    scheme = String.to_atom(uri.scheme)
    host = uri.host
    port = uri.port || 80
    # port = 80
    path = Path.join(["/", uri.path || "/", endpoint])

    headers = [
      {"Accept", "application/json"}
    ]

    body = nil

    require Logger
    b = "#{host}:#{port}"
    Logger.info("#{method}: #{scheme}://#{Path.join(b, path)}")

    with_connection(scheme, host, port, [mode: :passive], fn conn ->
      with {:ok, conn, _ref} <- HTTP.request(conn, method, path, headers, body),
           {:ok, response} <- await_response(conn, timeout()),
           {:ok, result} <- Jason.decode(response) do
        {:ok, result}
      end
    end)
  end

  defp with_connection(scheme, host, port, opts, fun) do
    with {:ok, conn} <- HTTP.connect(scheme, host, port, opts) do
      try do
        fun.(conn)
      after
        HTTP.close(conn)
      end
    end
  end

  defp await_response(conn, timeout) do
    {:ok, conn, responses} = HTTP.recv(conn, 0, timeout)

    with {:error, :no_response_body} <- extract_body(responses) do
      await_response(conn, timeout)
    end
  end

  defp extract_body([]), do: {:error, :no_response_body}

  defp extract_body([response | rest]) do
    case response do
      {:status, _ref, status} when status >= 400 ->
        {:error, {:bad_code, status}}

      {:data, _ref, body} ->
        {:ok, body}

      {:error, _ref, error} ->
        {:error, {:http, error}}

      _ ->
        extract_body(rest)
    end
  end

  defp base_url do
    Application.get_env(:carbon_watcher, CarbonWatcher.Api, [])
    |> Keyword.fetch!(:url)
    |> URI.parse()
  end

  defp timeout do
    Application.get_env(:carbon_watcher, CarbonWatcher.Api, [])
    |> Keyword.fetch!(:timeout)
  end
end
