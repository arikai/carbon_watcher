defmodule CarbonWatcher.BacksyncPlanner do
  @moduledoc "One-shot GenServer that plans jobs for syncing missing historical data"
  use GenServer, restart: :transient

  require Logger

  alias CarbonWatcher.SyncUtils
  alias CarbonWatcher.SyncWorker

  @type state :: any

  @spec start_link() :: GenServer.on_start()
  @spec start_link(any) :: GenServer.on_start()
  def start_link(arg \\ :noargs) do
    GenServer.start_link(__MODULE__, [arg])
  end

  @spec init(any) :: {:ok, state, {:continue, :plan_backsync}}
  def init(_noargs) do
    {:ok, nil, {:continue, :plan_backsync}}
  end

  @spec handle_continue(:plan_backsync, state) :: {:stop, :normal | any, state}
  def handle_continue(:plan_backsync, state) do
    with {:ok, from} <- fetch_sync_starting_date(),
         {:ok, to} <- SyncUtils.get_agg_measurement_timestamp(:min),
         {:ok, interval} <- interval(from, to) do
      interval
      |> Enum.map(&SyncWorker.new(%{at: &1}))
      |> Oban.insert_all()

      {:stop, :normal, state}
    else
      {:error, :bad_interval} ->
        {:stop, :normal, state}

      {:error, _} = error ->
        {:stop, error, state}
    end
  end

  defp fetch_sync_starting_date do
    key = :sync_starting_date

    case Application.get_env(:carbon_watcher, key) do
      nil ->
        {:ok, NaiveDateTime.utc_now()}

      datetime_str ->
        case Date.from_iso8601(datetime_str) do
          {:ok, date} ->
            NaiveDateTime.new(date, ~T[00:00:00])

          {:error, :invalid_format} ->
            Logger.error(
              "Invalid configuration: #{inspect(key)} = #{datetime_str} (must be datetime)"
            )

            {:error, {:invalid_config, key}}
        end
    end
  end

  defp interval(from, to) do
    case Timex.compare(from, to) do
      -1 ->
        interval =
          Timex.Interval.new(
            from: from,
            until: to,
            step: [minutes: 30]
          )

        {:ok, interval}

      _ ->
        {:error, :bad_interval}
    end
  end
end
