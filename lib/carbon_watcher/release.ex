defmodule CarbonWatcher.Release do
  @moduledoc "Release tasks to be used in production"

  @app :carbon_watcher

  @doc "Run all migrations or up to a version"
  @spec migrate(module | :all, version :: integer | :all) :: :ok | no_return()
  def migrate(repo \\ :all, version \\ :all) do
    run(:up, repo, version)
  end

  @doc "Rollback all migrations or down to a version"
  @spec rollback(module | :all, version :: integer | :all) :: :ok | no_return()
  def rollback(repo \\ :all, version \\ :all) do
    run(:down, repo, version)
  end

  defp run(command, repo, version) do
    repos =
      case repo do
        :all -> repos()
        repo -> [repo]
      end

    strategy =
      case version do
        :all -> [all: true]
        version -> [to: version]
      end

    for repo <- repos do
      {:ok, _, _} = Ecto.Migrator.with_repo(repo, &Ecto.Migrator.run(&1, command, strategy))
    end

    :ok
  end

  defp repos do
    Application.load(@app)
    Application.fetch_env!(@app, :ecto_repos)
  end
end
