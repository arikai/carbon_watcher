defmodule CarbonWatcher.SyncUtils do
  @moduledoc "Sync functionality-related utilities"

  alias CarbonWatcher.Repo

  @type agg_fun :: :min | :max

  @doc "Get aggregated timestamp of carbon measurements (e.g. min/max)"
  @spec get_agg_measurement_timestamp(agg_fun) :: {:ok, NaiveDateTime.t()} | {:error, any()}
  def get_agg_measurement_timestamp(agg_fun) when agg_fun in [:min, :max] do
    case Ecto.Adapters.SQL.query(Repo, "SELECT #{agg_fun}(timestamp) FROM carbon_intensity", []) do
      {:ok, %{rows: [[nil]]}} -> {:ok, NaiveDateTime.utc_now()}
      {:ok, %{rows: [[result]]}} -> {:ok, result}
      {:error, _} = err -> err
    end
  end

  @doc """
  Clip datetime to API-provided timestamps (HH:00:00 or HH:30:00)

    iex> CarbonWatcher.SyncUtils.clip_datetime(~N[2021-09-10 13:21:33Z])
    ~N[2021-09-10 13:00:00Z]

    iex> CarbonWatcher.SyncUtils.clip_datetime(~N[2021-09-10 13:49:22Z])
    ~N[2021-09-10 13:30:00Z]
  """
  @spec clip_datetime(NaiveDateTime.t()) :: NaiveDateTime.t()
  def clip_datetime(datetime = %NaiveDateTime{}) do
    datetime
    |> Map.update!(:minute, &(30 * div(&1, 30)))
    |> Map.merge(%{second: 0, microsecond: {0, 0}})
  end
end
