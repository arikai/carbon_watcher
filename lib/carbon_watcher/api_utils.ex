defmodule CarbonWatcher.ApiUtils do
  @moduledoc "`CarbonWatcher.Api`-related utility functions"

  @doc """
  Calculate API's period from datetime (considering as a start of a period).

    iex> CarbonWatcher.ApiUtils.to_period(~N[2021-09-10 00:00:00Z])
    1

    iex> CarbonWatcher.ApiUtils.to_period(~N[2021-09-10 13:30:00Z])
    28
  """
  @spec to_period(NaiveDateTime.t()) :: integer()
  def to_period(datetime = %NaiveDateTime{}) do
    datetime.hour * 2 + div(datetime.minute, 30) + 1
  end

  def extract_actual_intensity(response) do
    response
    |> Map.fetch!("data")
    |> CarbonWatcher.Utils.list_fetch_at(0)
    |> Map.fetch!("intensity")
    |> Map.fetch!("actual")
    |> case do
      measurement when is_integer(measurement) ->
        {:ok, measurement}

      nil ->
        {:error, :no_data}

      data ->
        {:error, {:invalid_data, data}}
    end
  rescue
    KeyError ->
      {:error, :no_data}
  end
end
