defmodule CarbonWatcher.SyncManagerWorker do
  @moduledoc "Oban job that plans sync jobs (see `SyncWorker`) for recent data"

  use Oban.Worker,
    queue: :sync_manager,
    priority: 0,
    max_attempts: 1

  alias CarbonWatcher.SyncWorker
  alias CarbonWatcher.SyncUtils

  @impl Oban.Worker
  def perform(_) do
    with {:ok, from} <- SyncUtils.get_agg_measurement_timestamp(:max) do
      until = NaiveDateTime.utc_now()

      Timex.Interval.new(
        from: from,
        until: until,
        left_open: false,
        right_open: true,
        step: [minutes: 30]
      )
      |> Enum.map(&SyncWorker.new(%{at: SyncUtils.clip_datetime(&1)}))
      |> Oban.insert_all()

      :ok
    end
  end
end
