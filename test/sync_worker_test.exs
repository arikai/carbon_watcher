defmodule CarbonWatcher.SyncWorkerTest do
  use ExUnit.Case

  alias CarbonWatcher.Repo

  @carbon_intensity 42

  import CarbonWatcher.WorkerTestUtils

  describe "perform/1" do
    test "works correctly (even repeatedly)" do
      datetime = ~N[2020-01-01T00:00:00]
      assert not match?({:error, _}, sync_point(datetime))
      assert not match?({:error, _}, sync_point(datetime))

      result =
        Ecto.Adapters.SQL.query(Repo, "SELECT value FROM carbon_intensity WHERE timestamp = $1", [
          datetime
        ])

      assert match?({:ok, %{rows: [[@carbon_intensity]]}}, result)
    end

    test "correct wraparound" do
      assert {:error, :max_attempts_wraparound} ==
               sync_point(~N[2020-01-01T00:00:00], attempt: 20)
    end

    test "no actual measurement" do
      assert match?({:error, :no_data}, sync_point(~N[2022-01-01T00:00:00]))
    end

    test "no data" do
      assert match?({:error, :no_data}, sync_point(~N[3000-01-01T00:00:00]))
    end

    test "empty response" do
      assert match?({:error, :no_data}, sync_point(~N[5000-01-01T00:00:00]))
    end

    test "incorrect data" do
      assert match?({:error, {:invalid_data, _}}, sync_point(~N[7000-01-01T00:00:00]))
    end
  end
end
