defmodule CarbonWatcher.ApiTest do
  use ExUnit.Case
  doctest CarbonWatcher.ApiUtils
  doctest CarbonWatcher.Api

  describe "get_intensity_at_datetime/1" do
    test "works correctly" do
      assert match?(
               {:ok, _},
               CarbonWatcher.Api.get_intensity_at_datetime(~N[2020-01-01T00:00:00])
             )
    end
  end
end
