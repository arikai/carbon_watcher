defmodule CarbonWatcherTest.Api.MockServer do
  def child_spec(opts \\ []) do
    default_opts = [
      scheme: env(:scheme),
      port: env(:port),
      plug: __MODULE__
    ]

    Plug.Cowboy.child_spec(Keyword.merge(default_opts, opts))
  end

  defp env(key) do
    Application.fetch_env!(:carbon_watcher, CarbonWatcher.Api)
    |> Keyword.fetch!(key)
  end

  # Plug

  use Plug.Router

  plug(:match)
  plug(:dispatch)

  get("/intensity/date/:date/:period", do: get_intensity(conn, date, period))

  def get_intensity(conn, "2020-01-01", "1") do
    body = %{
      "data" => [
        %{
          "from" => "2020-01-01T00:00Z",
          "intensity" => %{"actual" => 42, "forecast" => 42, "index" => "high"},
          "to" => "2020-01-01T00:30Z"
        }
      ]
    }

    send_resp(conn, 200, Jason.encode!(body))
  end

  def get_intensity(conn, "2022-01-01", "1") do
    body = %{
      "data" => [
        %{
          "from" => "2022-01-01T00:00Z",
          "intensity" => %{"actual" => nil, "forecast" => 42, "index" => "high"},
          "to" => "2022-01-01T00:30Z"
        }
      ]
    }

    send_resp(conn, 200, Jason.encode!(body))
  end

  def get_intensity(conn, "3000-01-01", "1") do
    body = %{"data" => []}
    send_resp(conn, 200, Jason.encode!(body))
  end

  def get_intensity(conn, "5000-01-01", "1") do
    body = %{}
    send_resp(conn, 200, Jason.encode!(body))
  end

  def get_intensity(conn, "7000-01-01", "1") do
    body = %{
      "data" => [
        %{
          "from" => "7000-01-01T00:00Z",
          "intensity" => %{"actual" => "forty two", "forecast" => 42, "index" => "high"},
          "to" => "7000-01-01T00:30Z"
        }
      ]
    }

    send_resp(conn, 200, Jason.encode!(body))
  end

  def get_intensity(conn, date, period) do
    time_from = from_period(String.to_integer(period))
    time_to = Time.add(time_from, 30 * 60)
    actual = :rand.uniform(250) + 50
    forecast = actual - 25 + :rand.uniform(50)

    body = %{
      "data" => [
        %{
          "from" => "#{format_datetime(date, time_from)}",
          "intensity" => %{
            "actual" => actual,
            "forecast" => forecast,
            "index" => "high"
          },
          "to" => "#{format_datetime(date, time_to)}"
        }
      ]
    }

    send_resp(conn, 200, Jason.encode!(body))
  end

  defp from_period(period) do
    {:ok, time} = Time.new(div(period - 1, 2), 30 * rem(period - 1, 2), 0)
    time
  end

  defp format_datetime(date, time) do
    time_str = String.slice("#{time}", 0, 5)
    "#{date}T#{time_str}Z"
  end
end
