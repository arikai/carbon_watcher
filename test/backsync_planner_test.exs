defmodule CarbonWatcher.BacksyncPlannerTest do
  use ExUnit.Case

  alias CarbonWatcher.Repo
  alias CarbonWatcher.BacksyncPlanner

  import CarbonWatcher.WorkerTestUtils

  test "correctly plans backsync" do
    datetime_start = ~N[1990-01-01T00:00:00]
    datetime_end = NaiveDateTime.add(datetime_start, 24 * 60 * 60)

    Application.put_env(
      :carbon_watcher,
      :sync_starting_date,
      to_string(NaiveDateTime.to_date(datetime_start))
    )

    sync_point(datetime_end)

    Process.flag(:trap_exit, true)
    {:ok, pid} = BacksyncPlanner.start_link()
    assert_receive {:EXIT, ^pid, :normal}, 60_000

    result =
      Ecto.Adapters.SQL.query(
        Repo,
        "SELECT count(*) FROM oban_jobs WHERE (args->>'at')::timestamp between $1 and $2",
        [datetime_start, datetime_end]
      )

    assert match?({:ok, %{rows: [[48]]}}, result)
  end
end
