defmodule CarbonWatcher.SyncWorkerManagerTest do
  use ExUnit.Case

  alias CarbonWatcher.Repo
  alias CarbonWatcher.SyncManagerWorker
  alias CarbonWatcher.SyncUtils

  import CarbonWatcher.WorkerTestUtils

  describe "perform/1" do
    test "correctly adds data" do
      datetime_end = NaiveDateTime.utc_now()
      datetime_start = NaiveDateTime.add(datetime_end, -24 * 60 * 60)
      sync_point(datetime_start)
      perform(SyncManagerWorker)

      result =
        Ecto.Adapters.SQL.query(
          Repo,
          "SELECT count(*) FROM oban_jobs WHERE (args->>'at')::timestamp between $1 and $2",
          [SyncUtils.clip_datetime(datetime_start), SyncUtils.clip_datetime(datetime_end)]
        )

      assert match?({:ok, %{rows: [[49]]}}, result)
    end
  end
end
