"test/**/*.exs"
|> Path.wildcard()
|> Enum.reject(&String.ends_with?(&1, "_test.exs"))
|> Enum.reject(&(&1 == "test/test_helper.exs"))
|> Enum.map(&Code.require_file/1)

Supervisor.start_link([CarbonWatcherTest.Api.MockServer], strategy: :one_for_one)

ExUnit.start()
