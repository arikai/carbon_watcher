defmodule CarbonWatcher.WorkerTestUtils do
  alias CarbonWatcher.SyncWorker

  def sync_point(datetime, extra_opts \\ []) do
    %{"at" => to_string(datetime)}
    |> SyncWorker.new()
    |> Ecto.Changeset.apply_changes()
    |> Map.merge(Map.new(extra_opts))
    |> SyncWorker.perform()
  end

  def perform(mod, args \\ %{}) do
    args
    |> mod.new()
    |> Ecto.Changeset.apply_changes()
    |> mod.perform()
  end
end
