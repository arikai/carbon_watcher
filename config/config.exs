import Config

config :carbon_watcher, ecto_repos: [CarbonWatcher.Repo]

config :carbon_watcher, CarbonWatcher.Repo,
  database: "carbon_watcher",
  hostname: "timescaledb",
  port: 5432,
  username: "postgres",
  password: "postgres"

config :carbon_watcher,
  sync_starting_date: "2021-09-01"

config :carbon_watcher, Oban,
  repo: CarbonWatcher.Repo,
  queues: [
    # Works as an imprecise rate limiter as well:
    # since there's no API call limits, this will do until there's more info
    sync: 100,
    sync_manager: 1
  ],
  plugins: [
    # Store jobs for a month: about 1500 metric records
    {Oban.Plugins.Pruner, max_age: 30 * 24 * 60 * 60},
    {Oban.Plugins.Cron,
     crontab: [
       {"5 * * * *", CarbonWatcher.SyncManagerWorker},
       {"35 * * * *", CarbonWatcher.SyncManagerWorker}
     ]}
  ]

config :carbon_watcher, CarbonWatcher.Api,
  url: "https://api.carbonintensity.org.uk",
  timeout: 60_000

import_config "#{Mix.env()}.exs"
