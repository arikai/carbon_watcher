import Config

scheme = :http
port = 8080

config :carbon_watcher, CarbonWatcher.Api,
  url: "#{scheme}://localhost:#{port}",
  scheme: :http,
  port: port,
  timeout: 60_000

config :carbon_watcher, CarbonWatcher.Repo,
  database: "carbon_watcher_test",
  hostname: "timescaledb_test"

config :carbon_watcher, Oban, queues: false, plugins: false
