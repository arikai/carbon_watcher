import Config

config :carbon_watcher,
  sync_starting_date: "2021-09-01"

config :carbon_watcher, CarbonWatcher.Repo,
  hostname: "localhost",
  port: 55432
