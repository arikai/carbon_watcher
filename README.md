# CarbonWatcher

The test challenge

Description can be found in [TASK.md](TASK.md)

## Details

The task was solved using job planning via Oban library. API calls were made with process-less Mint HTTP-client.

The moving parts are:
- [API Client](./lib/carbon_watcher/api.ex): Thin wrapper around Mint for the Carbon Intensity API
- [SyncWorker](./lib/carbon_watcher/sync_worker.ex): Oban worker that syncs data of specific datapoint (half-hour period of exact date)
- [SyncManagerWorker](./lib/carbon_watcher/sync_manager_worker.ex): Periodic Oban job that [ones per half-hour](./config/config.exs#L28-29) plans new SyncWorker jobs
- [BacksyncPlanner](./lib/carbon_watcher/backsync_planner.ex): One-shot process that plans missing measurements in historical data (i.e. those that must exist between [setting-defined](./config/config.exs#L12-13) [zero-hour and the oldest measurement](./lib/carbon_watcher/backsync_planner.ex#L25-27))

As a storage, [TimescaleDB](https://www.timescale.com/), PostgreSQL-based extension to turn Postgres into time-series database, which is suitable for the kind of data we're dealing with here (although the volume is quite small in this case: ~17K rows per year).

All code is covered with tests and additionally checked for formatting (via `mix format`), code style (via `credo`), type discrepancies (via `dialyxir/dialyzer`) and documentation coverage (via `doctor`).

For testing [simple cowboy-plug-based mock server](./test/api/mock_server.exs) was created with some predefined and generic answers

All relevant scripts are implemented as a Makefile tasks (see [Makefile](Makefile))

## Requirements
- GNU Make
- Bash
- Docker and Docker Compose

And extra requirements for building the project locally:
- Elixir 1.12
- Erlang 23+

## How To

### Start app with required services from image (via docker-compose)

    $ make

### The same, but build image from sources

    $ make all_build

### Run tests with test database (via docker-compose)

    $ make wc_test

### Run all available checks (inc. `wc_test`)

    $ make all_checks

## Notes

### Extra features

Dockerfiles for [prod](Dockerfile) and [test](Dockerfile.test) images are provided in a project.
Immediate run and testing infrastructures are provided in [docker-compose.yaml](docker-compose.yaml)

[Gitlab CI/CD](.gitlab-ci.yml) was added as well for automatic checking and image building and publishing.

Some features were deliberately ommited, since they're usually infrastructure-dependent or based on case at hand:
- healthcheck route
- metrics gathering (as well as custom metrics)
- custom logs formatting and export (e.g. to Logstash)
