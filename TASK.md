# How good is the air?

## Intro

With development of enhanced monitoring systems everywhere, we’re now able to get lots of information about things around us. Lots of facilities offer API to access and leverage data.  For this test exercise we will use one of those APIs and play with it’s data.


## Usage of The API

The API we are going to use will be the one from UK National Grid, which reports CO2 emissions related to electricity production – https://carbon-intensity.github.io/api-definitions/#carbon-intensity-api-v2-0-0. Our goal is to store actual intensity values they provide with as much of time-wise granularity as possible so we can leverage this data later (to build models, to plot graphs, etc.). Just one value with a time point along.


## Implementation details

The implementation should resemble “real software ready for production”. Which means it’s written in a conscious way and well tested. Something you would be fine with to file as a PR. It should refill missing data in case of own downtime or Internet connection going down.

Language of implementation can be anything. We prefer Elixir as it’s our main language we use in our team but if you don’t feel comfortable yet with it it’s fine to use whatever language you want. The way you store the data is also completely up to you – database, kafka, files, etc. But be ready to explain your choices during the interview. ;)


## Expectations

As previously mentioned we expect code to be working, readable, logical, covered with tests. Ideally a ready solution should be available to us via GitHub with instructions how to run it. It also would be nice if git commit history is in place. There’s no need to provide anything other than what is requested in the task but if you want to do so it’s absolutely ok. We don’t expect you to spend more than 3-4 hours with this task. It means you can stop doing it if you think it takes too long and just write what you would also do if you didn’t stop.
